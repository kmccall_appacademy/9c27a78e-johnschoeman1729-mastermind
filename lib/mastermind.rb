require 'byebug'
class Code
  PEGS = {
    "B" => :blue,
    "G" => :green,
    "O" => :orange,
    "P" => :purple,
    "R" => :red,
    "Y" => :yellow
  }

  attr_reader :pegs

  def self.parse(string)
    raise if !(string.downcase =~ /[^rgbyop]/).nil?
    pegs = string.split("").map { |peg| PEGS[peg.upcase] }
    Code.new(pegs)
  end

  def self.random
    pegs = []
    4.times { pegs << PEGS.values.sample }
    Code.new(pegs)
  end

  def initialize(pegs)
    @pegs = pegs
  end

  def [](index)
    @pegs[index]
  end

  def exact_matches(code)
    matches = 0
    0.upto(3) do |idx|
      matches += 1 if @pegs[idx] == code[idx]
    end
    matches
  end

  def near_matches(code)
    near_matches = 0
    temp_pegs = @pegs.dup # delete matched pegs to avoid overcounting.

    code.pegs.each do |peg|
      if temp_pegs.include?(peg)
        near_matches += 1
        temp_pegs.delete_at(temp_pegs.index(peg))
      end
    end

    near_matches - exact_matches(code)
  end

  def ==(code)
    (code.is_a? Code) ? @pegs == code.pegs : false
  end
end

class Game
  DASH = "-" * 22

  attr_reader :secret_code
  attr_accessor :guess_count

  def initialize(secret_code = nil)
    @secret_code = secret_code || Code.random
    @guess_count = 0
  end

  def play
    puts "Welcome to Mastermind!"
    puts DASH
    puts "Your options are: rgbyop"
    loop do
      guess = play_turn
      if @secret_code == guess
        puts "Congratulations! You Are A Mastermind!"
        puts "Secret Code: #{@secret_code.pegs.join(" ")}"
        break
      elsif @guess_count >= 10
        puts "Game Over! You failed to find the Secret Code."
        puts "Secret Code: #{@secret_code.pegs.join(" ")}"
        break
      else
        puts "Incorrect Code"
        puts "Guess Count: #{@guess_count}"
        display_matches(guess)
        puts DASH
      end
    end
  end



  def play_turn
    @guess_count += 1
    get_guess
  end

  def get_guess
    print "Enter your guess: "
    Code.parse(gets.chomp)
  end

  def display_matches(code)
    near_matches = @secret_code.near_matches(code)
    exact_matches = @secret_code.exact_matches(code)
    puts "exact matches: #{exact_matches}"
    puts "near matches:  #{near_matches}"
  end

  def won?(guess)
    @secret_code == guess
  end

end

if __FILE__ == $PROGRAM_NAME
  game = Game.new
  game.play
end
